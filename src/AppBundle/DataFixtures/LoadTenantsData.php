<?php
namespace AppBundle\DataFixtures;

use AppBundle\Entity\Tenant;
use Doctrine\Common\DataFixtures\FixtureInterface;
//use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadTenantsData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $data = [
            ['id' => null, 'server' => '127.0.0.1', 'database' => 'client1', 'username' => 'root', 'password' => 'root'],
            ['id' => null, 'server' => '127.0.0.1', 'database' => 'client2', 'username' => 'root', 'password' => 'root'],
            ['id' => null, 'server' => '127.0.0.1', 'database' => 'client3', 'username' => 'root', 'password' => 'root']
        ];

        foreach($data as $tenant) {
            $object = Tenant::fromArray($tenant);
            $manager->persist($object);
        }

        $manager->flush();
    }
}