<?php
namespace AppBundle\Client;
use Doctrine\ORM\Mapping as ORM;

class Invoice 
{
    /** @var  string */
    protected $id;
    /** @var integer */
    protected $number;

    /** @var string */
    protected $company;

    /**
     * @param $array
     * @return Invoice
     */
    public static function fromArray($array)
    {
        return new self($array['id'], $array['number'], $array['company']);
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Invoice
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return Invoice
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }
}
