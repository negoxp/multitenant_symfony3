<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Client\Invoice;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class InvoiceController extends Controller
{

    /**
     * @Route("/invoice/", name="invoice_index")
     */
    public function indexAction(Request $request)
    {

        $invoices = [];
        $name = null;

        if($this->get('doctrine.dbal.tenant_connection')->isConnected())
        {
            $em = $this->getDoctrine()->getManager('tenant');
            $invoices = $em->getRepository('AppBundle:Invoice')->findAll();
            $name = $this->get('doctrine.dbal.tenant_connection')->getDatabase();
        }

        return $this->render('default/index.html.twig', [
            'tenants' => $this->get('app_bundle.repository.tenant')->findAll(),
            'name' => $name,
            'invoices' => $invoices 
        ]);
    }

    /**
     * @Route("/invoice/add", name="invoice_add")
     */
    public function addAction(Request $request)
    {
    	$invoices = [];
        $name = null;

        if($this->get('doctrine.dbal.tenant_connection')->isConnected())
        {
            $em = $this->getDoctrine()->getManager('tenant');
            $invoices = $em->getRepository('AppBundle:Invoice')->findAll();
            $name = $this->get('doctrine.dbal.tenant_connection')->getDatabase();

            $invoice = new Invoice;
            $invoice->setNumber(uniqid());
        	$form = $this->createFormBuilder($invoice)
            ->add('company', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Invoice'))
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
		        $invoice = $form->getData();
		        $em->persist($invoice);
		        $em->flush();
		        return $this->redirectToRoute('homepage');
		    }

        	return $this->render('invoice/new.html.twig', array(
            	'form' => $form->createView(),
        	));

        }else{
        	return $this->redirectToRoute('homepage');
        }
    
    }

    /**
     * @Route("/invoice/delete/{invoiceId}", name="invoice_delete")
     */
    public function deleteAction($invoiceId)
    {
        $invoices = [];
        $name = null;

        if($this->get('doctrine.dbal.tenant_connection')->isConnected())
        {
            $em = $this->getDoctrine()->getManager('tenant');
            $invoice = $em->getRepository('AppBundle:Invoice')->findOneById($invoiceId);
            $name = $this->get('doctrine.dbal.tenant_connection')->getDatabase();

            $em->remove($invoice);
            $em->flush();
            return $this->redirectToRoute('homepage');

        }else{
            return $this->redirectToRoute('homepage');
        }
    }


    /**
     * @Route("/invoice/update/{invoiceId}", name="invoice_update")
     */
    public function updateAction($invoiceId, Request $request)
    {
        $invoices = [];
        $name = null;

        if($this->get('doctrine.dbal.tenant_connection')->isConnected())
        {
            $em = $this->getDoctrine()->getManager('tenant');
            $name = $this->get('doctrine.dbal.tenant_connection')->getDatabase();

            $invoice = $em->getRepository('AppBundle:Invoice')->findOneById($invoiceId);
            //$invoice->setNumber(uniqid());
            $form = $this->createFormBuilder($invoice)
            ->add('company', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Update Invoice'))
            ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $invoice = $form->getData();
                $em->persist($invoice);
                $em->flush();
                return $this->redirectToRoute('homepage');
            }

            return $this->render('invoice/new.html.twig', array(
                'form' => $form->createView(),
            ));

        }else{
            return $this->redirectToRoute('homepage');
        }
    }






}
